import React from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from '../Components/Header/Header';
import App from "../App";
import List from '../Components/List/List';

const RoutesConfig = () => {
    return(
    <Router>
        <Header />
        <Routes>
                <Route path='/' element={<App />} />
                <Route path='/boards' element={<App />} />
                <Route path='/boards/:id' element={<List />} />
        </Routes>
    </Router>
    )
}

export default RoutesConfig

