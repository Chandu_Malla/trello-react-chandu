import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import Header from './Components/Header/Header'
import RoutesConfig from './Routes/RoutesConfig'
import { ChakraProvider } from '@chakra-ui/react'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ChakraProvider>
      <RoutesConfig />
    </ChakraProvider>
  </React.StrictMode>,
)

