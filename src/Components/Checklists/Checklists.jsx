import { useEffect, useState } from 'react'
import axios from 'axios'


import {
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverHeader,
    PopoverBody,
    PopoverFooter,
    PopoverArrow,
    PopoverCloseButton,
    PopoverAnchor,
    Button,
    Input, InputGroup, InputRightElement, 
    IconButton,
    Box,Checkbox,
    List, Text, HStack, space, CloseButton
  } from '@chakra-ui/react'
  import { AddIcon, DeleteIcon, CheckCircleIcon, SmallCloseIcon, PlusSquareIcon } from '@chakra-ui/icons'

  import {
    Alert,
    AlertIcon,
    AlertTitle,
    AlertDescription,
  } from '@chakra-ui/react'


const Checklist = ({ cardId }) => {

  const apiKey = import.meta.env.VITE_TRELLO_API_KEY;
  const apiToken = import.meta.env.VITE_TRELLO_API_TOKEN;

  const [checkListData, setCheckListData] = useState(null)
  const [checklistTitle, setCheckListTitle] = useState('')


  const [checkItemName, setCheckItemName] = useState('')

  const [addCheckItem, setAddCheckItem] = useState(false)
 

  const [showAlert, setShowAlert] = useState(false); 
  const [alertMessage, setAlertMessage] = useState('')

  const closeAlert = () => {
    setShowAlert(!showAlert);
  }

  useEffect(() => {

    axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`)
    .then(response => {

      const newCheckListData = response.data
      setCheckListData(newCheckListData)

    })
    .catch(err => console.error(err)); 

  }, [cardId])


  const  updateCheckListData = () => {
    const newCheckListData = {'name': checklistTitle}
    setCheckListData([...checkListData, newCheckListData])
}

  const handleCheckListTitle = (event) => {
    setCheckListTitle(event.target.value)
  }

  const postCheckListTitle = () => {
  
    axios.post(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}&name=${checklistTitle}`)
         .then(text => console.log(`Card ${checklistTitle} is added to the board`))
         .catch(err => console.error(err))

         updateCheckListData()
         setCheckListTitle('')
  }


  const deleteCheckListData = (checkListId) => {
    setCheckListData((prevCheckListData) => prevCheckListData.filter((checkList) => {
        return checkList.id !== checkListId
    }))     
}
  

  const deleteCheckList = (checkListId) => {
   
     axios.delete(`https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${apiToken}`)
          .then(text => console.log(`Checklist ${checklistTitle} is deleted from the board`))
          .catch(err => console.error(err))

      deleteCheckListData(checkListId)
      setCheckListTitle('')
  }


  const handleCheckItemName = (event) =>  {
    setCheckItemName(event.target.value)
  }

  const postCheckItem = ( checklistId ) => {

    axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${apiToken}&name=${checkItemName}`)
    .then(text => console.log(`Card ${checklistTitle} is deleted from the checkList`))
    .catch(err => console.error(err))


    const updatedCheckListData = checkListData.map((checkList) => {
      if (checkList.id === checklistId) {
        checkList.checkItems.push({name: checkItemName})
      }
      return checkList
    })

    setCheckItemName('')

    setCheckListData( updatedCheckListData )

    
  }


  const deleteCheckItem = (checkListId, checkItemId) => {

    axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`)
    .then(text => console.log(`Card ${checklistTitle} is deleted from the checkList`))
    .catch(err => console.error(err))
    
  

    const updatedCheckList = [...checkListData].map((checkList) => {
      if (checkList.id === checkListId) {
        const updatedCheckItems = checkList.checkItems.filter(item => item.id !== checkItemId);
        checkList.checkItems = updatedCheckItems;
      }
      return checkList;
    })

     setCheckListData( updatedCheckList )

  }


  const putCheckItemsValue = (checklistId, checkItemId, checkItemState) => {

      const state = checkItemState === 'complete' ? 'incomplete' : 'complete'

      axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=${state}`) 
      .then(text => {
        setAlertMessage(`${checkItemId} checkItem state is ${state}`);
        setShowAlert(true); 
      })
      .catch(err => console.error(err))

      toggleCheckItemValue(checklistId, checkItemId, checkItemState)

  }

    const toggleCheckItemValue = (checklistId, checkItemId, currentState) => {
      const updatedChecklists = checkListData.map((checklist) => {
        if (checklist.id === checklistId) {
          const updatedCheckItems = checklist.checkItems.map((item) => {
            if (item.id === checkItemId) {
              return { ...item, state: currentState === 'complete' ? 'incomplete' : 'complete' };
            }
            return item;
          });
          return { ...checklist, checkItems: updatedCheckItems };
        }
        return checklist;
      });
  
      setCheckListData(updatedChecklists)
      
  }
  



    return(<>
          <Popover >
          <PopoverTrigger>
            <Button>CHECKLIST</Button>
          </PopoverTrigger>
          <PopoverContent maxH="300px" w='30vw' overflowY="auto">
            <PopoverArrow />
            <PopoverCloseButton />

            <PopoverHeader>
            {showAlert &&
               <Alert status='success' marginBottom='1rem'>
                <AlertIcon />
                {alertMessage}
                <CloseButton onClick={closeAlert} />
                </Alert> }
             
              Create a Checklist!</PopoverHeader>
            <PopoverBody>
         
              <InputGroup >
                    <Input color='black' type='text' value={checklistTitle} onChange={(event) => handleCheckListTitle(event)}/>
                    <InputRightElement>
                       
                        <IconButton
                            variant='ghost'
                            icon={<CheckCircleIcon />}
                            onClick={postCheckListTitle}
                        />  
                        
                    </InputRightElement>

                </InputGroup>
               
                {
                  checkListData && checkListData.map((checkList,index) => (
                    <Box key={checkList.id} m='1rem'>

                    <Box>
                    <HStack display='flex' justifyContent='space-between'>
                      <Button >{checkList.name}</Button>

                      { addCheckItem ?
                           <InputGroup display='flex' justifyContent='space-between'>
                           
                            <Input type='text'
                                  id={index}
                                  value={checkItemName}
                                  onChange={(event) => handleCheckItemName(event)} 
                            />  

                            <IconButton 
                              onClick={() => postCheckItem(checkList.id)} 
                              icon={<PlusSquareIcon />}                               
                            />

                            </InputGroup>
                            :
                            <IconButton 
                            onClick={() => setAddCheckItem(!addCheckItem)}
                            icon={<AddIcon />}
                            />}


                      <IconButton
                          onClick={() => deleteCheckList(checkList.id)}
                          icon={<DeleteIcon />}
                      />  
                    </HStack>

                    </Box>


                          <Box>

                            {checkList.checkItems && checkList.checkItems.map((checkItem) => (
                              <Box key={checkItem.id} display="flex" alignItems="center">
                                <Checkbox value={checkItem.name} id={checkItem.id}
                                    isChecked={checkItem.state === 'complete'}
                                    onChange={() => putCheckItemsValue(checkList.id, checkItem.id, checkItem.state)} />
                                <HStack>
                                  <Text ml={2}>{checkItem.name}</Text>
                                  <IconButton
                                          variant='ghost'
                                          icon={<SmallCloseIcon />}
                                          onClick={() => deleteCheckItem(checkList.id, checkItem.id)}
                                      />  
                                </HStack>
                              </Box>
                            ))}
                        </Box>

                       
                    </Box>            
                  )) 
                }
            </PopoverBody>
         
          </PopoverContent>
        </Popover>
    </>
    )
    }

export default Checklist