import { Text, Box, SimpleGrid, InputGroup, InputLeftElement, Input, Button, Flex, HStack, IconButton} from '@chakra-ui/react'
import { AddIcon, CloseIcon } from '@chakra-ui/icons'

import axios, { HttpStatusCode } from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import Card from '../Card/Card'

const List = () => {

    const { id } = useParams()

    const [listsData, setListsData] = useState(null)
    const [listName, setListName] = useState('')
    const [deleteListId, setDeleteListId] = useState(null)

    const apiKey = import.meta.env.VITE_TRELLO_API_KEY;
    const apiToken = import.meta.env.VITE_TRELLO_API_TOKEN;  


    useEffect(() => {
        axios
          .get(`https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`)
          .then(response => setListsData(response.data))
          .catch(err => console.error(err)); 
    },[])

    const handleBoardName = (event) => {
        setListName(event.target.value)
    }


    const  updateListsData = () => {
        const newListData = {'name': listName}
        setListsData([...listsData, newListData])
    }
 

    const AddList = (e) => {
        e.preventDefault();
        axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${listName}&key=${apiKey}&token=${apiToken}`)
          .then(text => console.log(`List ${listName} is added to the board`))
          .catch(err => console.error(err))
        
        updateListsData()
        setListName('')
    }


    const deleteListsData = (listId) => {
        setListsData((listsData) => listsData.filter((list) => {
            return list.id !== listId
        }))     
    }

    const deletelist = (listId) => {

        axios.put(`https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${apiToken}&value=true`)
        .then(response => {
            console.log(`List ${listId} deleted from a board`);
        })
        .catch(err => console.error(err))
    

        deleteListsData(listId)
        setListName('')
        
    }          
                

    return(<>
  

        <SimpleGrid columns={7} spacing='3rem' marginTop='1rem' >
            
        <Box  height='30vh'  borderRadius='5px' 
                display='flex'   
                boxSizing='padding-box' p='1rem' bg='#4968d1' >
                <Flex flexDirection='column' alignItems='center' justifyContent='center'>
                <form onSubmit={AddList}>
                <InputGroup maxWidth='90%' bg='white' p='0'>
                    <InputLeftElement>
                    <AddIcon color='grey'/>
                    </InputLeftElement>
                    <Input type='text' w='80%' placeholder='Enter the List Name' onChange={handleBoardName} value={listName}
                    textAlign='center' fontSize={16} />
                </InputGroup>
                <Button variant='ghost' fontSize={16} maxWidth='100%' bg='white' type='submit'>Create a List</Button>
                </form>
                </Flex>
        </Box>


            {listsData && listsData.map((list) => (
                <Box key={list.id} height='auto' borderRadius='15px' bg='#4968d1'>
                    <HStack justifyContent='space-between'>
                    <Text marginTop='10px' marginLeft='10px' >{list.name}</Text>
                    <IconButton
                                    icon={<CloseIcon />}
                                    
                                    onClick={() => deletelist(list.id)}
                     />
                     </HStack>
                    <Card listId={list.id} /> 
                </Box> 
                ))}
           
        </SimpleGrid>
    </>
    )
}

export default List


