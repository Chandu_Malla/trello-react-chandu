import { Text, Box, SimpleGrid, InputLeftElement, Input, Button, Flex, IconButton, InputRightElement, InputGroup} from '@chakra-ui/react'
import { AddIcon, DeleteIcon, CheckCircleIcon } from '@chakra-ui/icons'

import axios from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import Checklist from '../Checklists/Checklists'

const Card = ( { listId }) => {


    const [CardsData, setCardsData] = useState(null)
    const [CardName, setCardName] = useState('')

    const [addCardBtnState, setAddCardBtnState] = useState(true)

    const [showChecklist, setShowChecklist] = useState(false)

    const [cardId, setCardId] = useState('')

    const apiKey = import.meta.env.VITE_TRELLO_API_KEY;
    const apiToken = import.meta.env.VITE_TRELLO_API_TOKEN;


    useEffect(() => {

        axios
          .get(`https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`)
          .then(response => {
            const newlistData = response.data
            setCardsData(newlistData)
          })
          .catch(err => console.error(err)); 

    },[listId])

    const handleCardName = (event) => {
        setCardName(event.target.value)
    }


    const  updateCardsData = () => {
        const newCardData = {'name': CardName}
        setCardsData([...CardsData, newCardData])
    }
 

    const AddCard = (e) => {
        e.preventDefault();// Me
        axios.post(`https://api.trello.com/1/cards?key=${apiKey}&token=${apiToken}&idList=${listId}&name=${CardName}`)
          .then(text => console.log(`Card ${CardName} is added to the board`))
          .catch(err => console.error(err))
        
          setAddCardBtnState(!addCardBtnState)

        updateCardsData()
        setCardName('')
    }


    const deleteCardsData = (CardId) => {
        setCardsData((CardsData) => CardsData.filter((Card) => {
            return Card.id !== CardId
        }))     
    }

    const deleteCard = (CardId) => {

        axios
        .delete(`https://api.trello.com/1/cards/${CardId}?key=${apiKey}&token=${apiToken}`)
        .then(response => {
            console.log(`Card ${CardId} deleted from a board`);
        })
        .catch(err => console.error(err))
    

        deleteCardsData(CardId)
        setCardName('')
        
    }          
             

    return (
        <>
        {/* <Flex h='75%' flexDirection='column' justifyContent='space-between'> */}
            {CardsData && CardsData.map(card => (
                <Box key={card.id} height='auto' marginBottom={'5px'} bg='#adcfee' 
                onClick={() => {
                    console.log('InputGroup clicked');
                    setCardId(card.id)
                    setShowChecklist(!showChecklist);
                }}>
                    <Flex justifyContent='space-between'>
                        <Text  marginTop='10px' marginLeft='10px' color='red'>{card.name}</Text>
                        <IconButton
                        //    key={card.id + 1}
                            onClick={() => deleteCard(card.id)}
                            icon={<DeleteIcon />}
                        />                    
                        </Flex>
                </Box>
            ))}   
            <form onSubmit={AddCard}>   
            {!addCardBtnState ?
             
                 <InputGroup >
                    <Input color='white' type='text' onChange={handleCardName}/>
                    <InputRightElement>
                        <IconButton
                            variant='ghost'
                            icon={<CheckCircleIcon />}
                            onClick={AddCard}
                        />
                    </InputRightElement>
                </InputGroup>
                : <Button onClick={() => setAddCardBtnState(!addCardBtnState)}>Add Card</Button>
                
            } 
            </form>
                {showChecklist && <Checklist cardId={cardId}/>}
            {/* </Flex>    */}
        </>
    );
}

export default Card
