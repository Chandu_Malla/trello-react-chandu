import React from 'react';
import { Container, Text, Box, Flex, Heading, Icon, Image } from "@chakra-ui/react";
import logo from '../../assets/trello-icon.svg'


const Header = () => {
  return (
    <>
      <Flex h='10vh' w='100vw' bgColor="darkblue"
       borderColor='red' color='pink' textAlign='center'
       justifyContent='center' alignItems='center'>
        <Box display={'flex'} justifyContent={'flex-start'}>
        <Image src={logo} alt="Logo" boxSize="50px" m={'auto'} />
      </Box>
        <Heading as='h1'>TRELLO APP</Heading>
      </Flex>
    </>
  );
};

export default Header;
