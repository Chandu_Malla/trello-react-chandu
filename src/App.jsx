import {useEffect, useState} from 'react'
import axios from 'axios'
import {  useNavigate } from 'react-router-dom'

import { Text, Box, Flex, Button, Input, SimpleGrid, InputLeftElement, InputGroup} from '@chakra-ui/react'
import { AddIcon } from '@chakra-ui/icons'

import List from './Components/List/List'




const App = () => {

  const [boardsData, setBoardsData] = useState(null)
  const [boardName, setBoardName] = useState('')
  const [selectedBoardId, setSelectedBoardId] = useState('')

  const apiKey = import.meta.env.VITE_TRELLO_API_KEY;
  const apiToken = import.meta.env.VITE_TRELLO_API_TOKEN;
  
  const API_URL = `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`

  const [error, setError] = useState(null)

  const navigate = useNavigate()

  useEffect(() => {
   
    axios.get(API_URL)
    .then(response => setBoardsData(response.data) )
    .catch(error => console.error('Error Fetching Data', error))

  }, [])


  const handleBoardName = (event) => {
      setBoardName(event.target.value)
  }

  const addDataToTheBoardsData = () => {
    const newBoardData = {'name': boardName}
    setBoardsData([...boardsData, newBoardData])
  }


  const handleSelectedBoardId = (id, name) => {
    navigate(`/boards/${id}`)
  }

 const AddBoard = (e) => {
  e.preventDefault();
    axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`)          
    .then(response => {
        return response.text();
      })
      .then(text => console.log(text))
      .catch(err => console.error(err))
    
    addDataToTheBoardsData()
    setBoardName('')
} 


const generateRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let index = 0; index < 6; index++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

  return(
     <>
    <SimpleGrid columns={4} spacing='3rem' boxSizing='padding-box' p='5rem' >

    <Box  height='30vh' w='100%' borderRadius='5px' bg={generateRandomColor}
    display='flex'   
    boxSizing='padding-box' p='1rem' >
       <form onSubmit={AddBoard}>
        Form using html
      <InputGroup w='90%' >
        <InputLeftElement>
         <AddIcon color='grey'/>
        </InputLeftElement>
        <Input type='text' w='80%' placeholder='Enter the Board Name' onChange={handleBoardName} value={boardName}
          textAlign='center' fontSize={16} />
      </InputGroup>
      <Button type='submit'  variant='ghost' fontSize={16} w='50%' >AddBoard</Button>
    </form>

    </Box>
    {boardsData && boardsData.map((board, index) => (
      <Box key={index} height='30vh' borderRadius='5px' bg={generateRandomColor}
      onClick={() => handleSelectedBoardId(board.id, board.name)}>
        <Text marginTop='10px' marginLeft='10px' color='white'>{board.name}</Text>
      </Box>
    ))}

    </SimpleGrid>
    </> 
  )
}

export default App